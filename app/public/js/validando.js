function validarNombre() {
  let nombre = document.getElementById("nombre");
  let errorNombre = document.getElementById("errorNombre");
  errorNombre.innerHTML = "";
  if (nombre.value.length > 40 || nombre.value.length < 2) {
    errorNombre.innerHTML = "Nombre no válido";
    nombre.style.backgroundColor = "#FD7575";
    return false;
  } else {
    errorNombre.innerHTML = "";
    nombre.style.backgroundColor = "white";
    return true;
  }
}

function validarEmail() {
  let inputEmail = document.getElementById("email");
  let posicionPunto = inputEmail.value.lastIndexOf(".");
  let posicionArroba = inputEmail.value.lastIndexOf("@");
  let errorEmail = document.getElementById("errorEmail");
  let dominio = inputEmail.value.substr(
    posicionPunto + 1,
    inputEmail.value.length
  );

  if (posicionPunto > 0) {
    if (
      dominio.length > 4 ||
      dominio.length < 2 ||
      posicionArroba > posicionPunto
    ) {
      inputEmail.style.backgroundColor = "#FD7575";
      errorEmail.innerHTML = "Email no válido";
      return false;
    } else {
      inputEmail.style.backgroundColor = "white";
      errorEmail.innerHTML = "";
      return true;
    }
  } else {
    inputEmail.style.backgroundColor = "#FD7575";
    errorEmail.innerHTML = "Email no válido";
    return false;
  }
}

function validarUsuario() {
  let usuario = document.getElementById("usuario");
  let errorUsuario = document.getElementById("errorUsuario");
  errorUsuario.innerHTML = "";
  if (usuario.value.length > 40 || usuario.value.length < 5) {
    errorUsuario.innerHTML = "Usuario no válido";
    usuario.style.backgroundColor = "#FD7575";
    return false;
  } else {
    errorUsuario.innerHTML = "";
    usuario.style.backgroundColor = "white";
    return true;
  }
}

function validarContrasenna() {
  let contrasenna = document.getElementById("contrasenna");
  let errorContrasenna = document.getElementById("errorContrasenna");
  errorContrasenna.innerHTML = "";
  if (contrasenna.value.length > 40 || contrasenna.value.length < 4) {
    errorContrasenna.innerHTML = "Contraseña no válida";
    contrasenna.style.backgroundColor = "#FD7575";
    return false;
  } else {
    errorContrasenna.innerHTML = "";
    contrasenna.style.backgroundColor = "white";
    return true;
  }
}

function validarAlEnviar() {
  var conseguido = validarNombre() && validarEmail();
  return conseguido;
}

function loginValidarEnviar() {
  var conseguido = validarUsuario() && validarContrasenna();
  return conseguido;
}
