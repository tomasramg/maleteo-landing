let desplegable = document.getElementById("desplegable");
let flechaMenu = document.getElementById("flechaMenu");
flechaMenu.onclick = desplegando;
desplegado = false;

function desplegando() {
  if (desplegado) {
    desplegable.style.display = "none";
    desplegado = false;
    flechaMenu.src = "/assets/img/arrowDown.png";
  } else {
    desplegable.style.display = "flex";
    desplegado = true;
    flechaMenu.src = "/assets/img/arrowUp.png";
  }
}
