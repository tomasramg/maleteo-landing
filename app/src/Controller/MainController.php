<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

Class MainController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }

    /**
     * @Route ("maleteo", name= "formularioMaleteo")
     */
    public function formularioMaleteo(Request $request, EntityManagerInterface $em)

    {
        $form = $this->createForm(MaleteoFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //GUARDAMOS LOS DATOS
            $demo = $form->getData();
            $em->persist($demo);
            $em->flush();
            return $this->redirectToRoute('base.html.twig');
        }
return $this->render(
    'base.html.twig'
,
[
    'formulario' => $form-> createView()

        ]);

    }
}