<?php


namespace App\form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaleteoFormType extends AbstractType
{
public function buildForm(FormBuilderInterface $builder, array $options)
{
   $builder->add('Nombre');
    $builder->add('Correo Electrónico');
    $builder->add('Ciudad');
}
 public function configureOptions(OptionsResolver $resolver)
 {
 $resolver->setDefaults(['data_class']);
 }
}
